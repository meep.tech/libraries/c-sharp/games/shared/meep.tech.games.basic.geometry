﻿using System;

namespace Meep.Tech.Games.Basic.Geometry {
  /// <summary>
  /// Direction constants
  /// </summary>
  public static class Directions {

    /// <summary>
    /// A valid direction
    /// </summary>
    public class Direction : IEquatable<Direction> {

      /// <summary>
      /// The id of the direction
      /// </summary>
      public int Value {
        get;
        private set;
      }

      /// <summary>
      /// The name of this direction
      /// </summary>
      public string Name {
        get;
        private set;
      }

      /// <summary>
      /// The x y z offset of this direction from the origin
      /// </summary>
      public Point Offset {
        get => Offsets[Value];
      }

      /// <summary>
      /// Get the oposite of this direction
      /// </summary>
      /// <param name="direction"></param>
      /// <returns></returns>
      public Direction Reverse {
        get {
          if (Equals(North)) {
            return South;
          }
          if (Equals(South)) {
            return North;
          }
          if (Equals(East)) {
            return West;
          }

          return East;
        }
      }

      /// <summary>
      /// Make the directions
      /// </summary>
      /// <param name="value"></param>
      /// <param name="name"></param>
      internal Direction(int value, string name) {
        Value = value;
        Name = name;
      }

      /// <summary>
      /// To string
      /// </summary>
      /// <returns></returns>
      public override string ToString() {
        return Name;
      }

      public override int GetHashCode() {
        return Value;
      }

      /// <summary>
      /// Equatable
      /// </summary>
      /// <param name="other"></param>
      /// <returns></returns>
      public bool Equals(Direction other) {
        return other.Value == Value;
      }

      /// <summary>
      /// Override equals
      /// </summary>
      /// <param name="obj"></param>
      /// <returns></returns>
      public override bool Equals(object obj) {
        return (obj != null)
          && !GetType().Equals(obj.GetType())
          && ((Direction)obj).Value == Value;
      }
    }

    /// <summary>
    /// Z+
    /// </summary>
    public static Direction North = new Direction(0, "North");

    /// <summary>
    /// X+
    /// </summary>
    public static Direction East = new Direction(1, "East");

    /// <summary>
    /// Z-
    /// </summary>
    public static Direction South = new Direction(2, "South");

    /// <summary>
    /// X-
    /// </summary>
    public static Direction West = new Direction(3, "West");

    /// <summary>
    /// All the directions in order
    /// </summary>
    public static Direction[] All = new Direction[4] {
      North,
      East,
      South,
      West
    };

    /// <summary>
    /// The cardinal directions. Non Y related
    /// </summary>
    public static Direction[] Cardinal = new Direction[4] {
      North,
      East,
      South,
      West
    };

    /// <summary>
    /// The coordinate directional offsets
    /// </summary>
    public static Point[] Offsets = new Point[4] {
      (0, 0, 1),
      (1, 0, 0),
      (0, 0, -1),
      (-1, 0, 0)
    };
  }
}
